<?php
//theme : Raha v0.2 beta 2
// This file defines the main theme functions.
/*
 * here we let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

//add text domain and translations directory for theme translation
load_theme_textdomain('raha', get_template_directory() . '/languages');


add_action('after_setup_theme', 'setup_theme');
function setup_theme(){
    load_theme_textdomain('raha', get_template_directory() . '/languages');
}


/*
 * Enable support for Post Thumbnails on posts and pages.
 */
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 700, 400, true );//here we are declaring the thumbnail size
// add Wordpress post format feature
add_theme_support( 'post-formats', array('image' ,'quote' ,'status' ) );

//add html5 theme support.
add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
) );

// adding menus with wp_nav_menu() function.
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header menu', 'raha' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );// registering the menu

// Register Sidebars
function raha_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'First footer sidebar', 'raha' ),
		'id'            => 'footer-sidebar1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'raha' ),
		'before_widget' => '<section id="%1$s" class="widget widget-container %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Second footer sidebar', 'raha' ),
		'id'            => 'footer-sidebar2',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'raha' ),
		'before_widget' => '<section id="%1$s" class="widget widget-container %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Third footer sidebar', 'raha' ),
		'id'            => 'footer-sidebar3',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'raha' ),
		'before_widget' => '<section id="%1$s" class="widget widget-container %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'raha_widgets_init' );


// add the costum header of wordpress
// so the theme uses the Wordpress intergated header input
$costumheader = array(
    'header-text'   => false,
    'flex-width'    => true,
    'flex-height'   => true,
    'width'         => 900,
    'height'        => 200,
    'default-image' => get_template_directory_uri() . '/image/header-bg.jpg',
);
add_theme_support( 'custom-header', $costumheader );


// costumizer API : the costumizer that adds the editable header picture, social media links and other stuff in the theme.
function raha_customize_register( $wp_customize ) {
// Add footer text and Meta tags
    // setting of header image activation defines the appearance of header image
    $wp_customize->add_setting( 'header-image-disable' , array(
    'default'     => false,
     'transport'   => 'refresh',
    ) );

    // setting to disable description in header
    $wp_customize->add_setting( 'description-disable' , array(
    'default'     => false,
     'transport'   => 'refresh',
    ) );


    // setting of footer text
    $wp_customize->add_setting( 'footer' , array(
    'default'     => __( 'Proudly powered by <a href="https://wordpress.org">Wordpress</a>.' , 'raha'),
     'transport'   => 'refresh',
    ) );

    // Settings of Meta tags
    $wp_customize->add_setting( 'meta-tags' , array(
    'default'     => '' ,
     'transport'   => 'refresh',
    ) );


    // section of footer text
    $wp_customize->add_section( 'footer-meta' , array(
    'title'      => __( 'Footer text & Meta tags', 'raha' ),
    'priority'   => 1002,
    ) );

    // control of header image activation
    $wp_customize->add_control(
        'header-image-disable',
        array(
            'label'    => __( ' Don\'t use header image', 'raha' ),
            'section'  => 'header_image',
            'settings' => 'header-image-disable',
            'type'     => 'checkbox',
            'description' => __( 'If checked the header image will not be displayed. so header will be text only.', 'raha' ),
        )
    );

    //control to disable description in header
    $wp_customize->add_control(
        'description-disable',
        array(
            'label'    => __( ' Don\'t use site description', 'raha' ),
            'section'  => 'header_image',
            'settings' => 'description-disable',
            'type'     => 'checkbox',
            'description' => __( 'If checked site description will not be shown beneath the title.', 'raha' ),
        )
    );


    // control of footer text
    $wp_customize->add_control(
        'footer',
        array(
            'label'    => __( 'Footer text', 'raha' ),
            'section'  => 'footer-meta',
            'settings' => 'footer',
            'type'     => 'textarea',
            'description' => __( 'You can use your own text in footer. can include copyright etc. (HTML supported)', 'raha' ),
        )
    );
    // Control of Meta tags
    $wp_customize->add_control(
        'meta-tags',
        array(
            'label'    => __( 'Addintional meta tags in Head', 'raha' ),
            'section'  => 'footer-meta',
            'settings' => 'meta-tags',
            'type'     => 'textarea',
            'description' => __( 'You can add addintional codes to your site\'s HEAD section. for example: Meta tags - Google vertification tags - addintional stylesheets etc.', 'raha' ),
        )
    );

// Add the social media bar
// it is floating on the right-bottom side of the screen and show a set of icons with links
//this setting is used to help the user declare whether he likes to use the bar or not, you can find a control  used with this setting below all other settings defined
    $wp_customize->add_setting( 'icon-bar-active' , array(
    'default'     => false ,
     'transport'   => 'refresh',
    ) );

// social media links
    // settings
    $wp_customize->add_setting( 'twitter' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );
    $wp_customize->add_setting( 'medium' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );
    $wp_customize->add_setting( 'github' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );
    $wp_customize->add_setting( 'email' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );
    $wp_customize->add_setting( 'linkedin' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );
    $wp_customize->add_setting( 'telegram' , array(
    'default'     => '',
     'transport'   => 'refresh',
    ) );

 // sections
    $wp_customize->add_section( 'social-media' , array(
    'title'      => __( 'Social media', 'raha' ),
    'priority'   => 1001,
    ) );


 // controls
	//here we define the switch for user to decide whether he likes to use the icon bar or not
    $wp_customize->add_control( 
        'icon-bar-active', 
        array(
            'label'    => __( 'Use social media icon bar', 'raha' ),
            'section'  => 'social-media',
	    'settings' => 'icon-bar-active',
            'type'     => 'checkbox',
            'description' => __('A floating bar containing social media icons and links on the left-bottom of the page.', 'raha' ),
        )
    );

    $wp_customize->add_control(
	'twitter',
	array(
		'label'    => __( 'Twitter', 'raha' ),
		'section'  => 'social-media',
		'settings' => 'twitter',
		'type'     => 'text',
        'description' => __( 'Insert Twitter username without @', 'raha' ),
	)
);
    $wp_customize->add_control(
    	'medium',
    	array(
    		'label'    => __( 'Medium', 'raha' ),
    		'section'  => 'social-media',
    		'settings' => 'medium',
    		'type'     => 'text',
            'description' => __( 'Insert Medium username without @', 'raha' ),
    	)
    );
    $wp_customize->add_control(
    	'github',
    	array(
    		'label'    => __( 'Github', 'raha' ),
    		'section'  => 'social-media',
    		'settings' => 'github',
    		'type'     => 'text',
            'description' => __( 'Insert Github username without @', 'raha' ),
    	)
    );
    $wp_customize->add_control(
    	'email',
    	array(
    		'label'    => __( 'Email', 'raha' ),
    		'section'  => 'social-media',
    		'settings' => 'email',
    		'type'     => 'text',
            'description' => __( 'Insert your Email address', 'raha' ),
    	)
    );
    $wp_customize->add_control(
    	'linkedin',
    	array(
    		'label'    => __( 'Linkedin', 'raha' ),
    		'section'  => 'social-media',
    		'settings' => 'linkedin',
    		'type'     => 'text',
            'description' => __( 'Insert your Linkedin profile url address', 'raha' ),
    	)
    );
    $wp_customize->add_control(
        'telegram',
        array(
            'label'    => __( 'Telegram', 'raha' ),
            'section'  => 'social-media',
            'settings' => 'telegram',
            'type'     => 'text',
            'description' => __( 'Insert your Telegram username without @', 'raha' ),
        )
    );

}

add_action( 'customize_register', 'raha_customize_register' ); // register the costumizer api

// return the Links section to the wordpress
add_filter( 'pre_option_link_manager_enabled', '__return_true' );
?>
