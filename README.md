# Raha
Raha is a free Wordpress theme and it fouces on regulation & standardization.
Raha is free and it will always be. Any contributions & personal or commercial uses & changes are allowed. (Read License section for more info)

[See a demo of Raha](http://poriyamh.github.io/Raha/)

Notice : **Raha is currently in Beta.** 
This means that you may face bugs while using it.

*You may help the progress by just using Raha and reporting the Bugs and problems. Also you can contribute to it's development.*

##To-Do list

Here is a list of things you can help Raha with. So if you want to help, feel free to fork and pull request!

  * Fix bug [*widget areas not showing in Wordpress customizer*](https://wordpress.org/support/topic/defined-sidebars-in-functionsphp-dont-show-up-in-wordpress-customizer).
  * **(Important)** Create an standard comments.php file
  * Write on Raha's Github wiki page.
  * Translate Raha .po file into more languages.
  * Add an option to show the site description below the title with a switch in Wordpress customizer.
  * Add footer navigation menu.

##Using Raha

###License
Raha theme is published under [GNU GENERAL PUBLIC LICENSE version 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) (GPLv3).

Here is a small summary about the license :

```
copyright (C) 2016 PoriyaMH (me@poriya.ir) 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
```

###How to download
If you want to use Raha on Wordpress, you can get the installation .zip file from the [release page](https://github.com/poriyaMH/Raha/releases).

*for installation help read FAQ.*

Otherwise you can clone the repository or download it.

###How to help
you can help the development of Raha by:

1. Contributing it's development.(contact me!)
2. Using it and reporting bugs.
3. Sharing Raha.

##FAQ
**What does Raha mean?**
Raha means *Free* in Persian.

**Can I use Raha?**
Yes! Raha is free and you can use it for any purpose that you want. But you may want to [check the license](https://github.com/poriyaMH/Raha#license).

**Is Raha available in Wordpress theme repository?**
Currently, Raha is in beta; But we will request adding it to the Wordpress theme repository as a stable release was completed.

**What languages is Raha available in?**
Currently, Raha is available in English and Persian.

If you think you can translate Raha into you own language feel free to use the .po file included in `/languages` folder and translate.
Then please fork the theme, add you .mo file and then request pull.

**How do I install Raha?**
First download an installation package from the [release page](https://github.com/poriyaMH/Raha/releases)(Note that installing a stable release is recommended).then follow [This Tutorial] (https://codex.wordpress.org/Using_Themes#Adding_New_Themes_using_the_Administration_Panels) for installation on Wordpress.

##Support
Feel free to contact me on twitter or send me an email to share your problems or ideas with me.

[Poriya on Twitter](https://twitter.com/poriyaMH)
 
my email: me@poriya.ir
