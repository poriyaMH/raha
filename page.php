<?php
//This is the page layout file. It defines the layout of the pages. Comments are not supported in pages
get_header();//Add the header of website from header.php
?>
    <main class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); // starts the loop ?>
            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
            		<header>
                        <h2 class="post-title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e('link to', 'raha')?> <?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    </header>
                    <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                    <div style="clear:both"></div>
                    <?php // check if the post has a Post Thumbnail assigned to it.
                            if ( has_post_thumbnail() ) {
                            	the_post_thumbnail();
                            }
                    ?>
            		<?php the_content( __('Keep reading <i class="fa fa-plus-circle"></i>', 'raha') ); ?>
            	</article>
            <?php endwhile; ?><?php //the main second loop?>
            <?php else : ?>

            	<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
            		<h1><?php _e('Sorry! not found.', 'raha') ?></h1>
            	</div>
            <?php endif; // ends the loop ?>
    </main>
<?php get_footer(); //add the footer section of the website ?>
