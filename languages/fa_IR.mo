��    -      �  =   �      �     �  V   �  +   L     x  
   �  !   �     �     �     �     �               %     1     I     P      \      }  !   �     �  (   �  '     .   +     Z     c     j  A   ~     �     �     �     �          #     (     1     F     N  �   h  P   �     F     I     Z     ]     e  �  h  !   2
  �   T
  K     .   S     �  )   �  
   �  &   �  (   �  )     !   A  %   c     �  '   �     �     �  C   �  >   /  B   n  1   �  B   �  I   &  7   p     �     �     �  k   �  &   N     u     �  2   �     �            '   $     L  @   Y  �   �  �   �     �     �     �     �     �         (          '                      %                                     "       
   *         +   ,            )       &                      $               !              #   	           -                   &laquo; Older Posts A floating bar containing social media icons and links on the left-bottom of the page. Add widgets here to appear in your sidebar. Addintional meta tags in Head Categories Edit <i class="fa fa-pencil"></i> Email First footer sidebar Follow on Github Follow on Linkedin Follow on Medium Follow on Twitter Footer text Footer text & Meta tags Github Header menu Insert Github username without @ Insert Medium username without @ Insert Twitter username without @ Insert your Email address Insert your Linkedin profile url address Insert your Telegram username without @ Keep reading <i class="fa fa-plus-circle"></i> Linkedin Medium Newer Posts &raquo; Proudly powered by <a href="https://wordpress.org">Wordpress</a>. Second footer sidebar Send Email to me Social media Sorry! Didn't find the post. Sorry! not found. Tags Telegram Third footer sidebar Twitter Use social media icon bar You can add addintional codes to your site's HEAD section. for example: Meta tags - Google vertification tags - addintional stylesheets etc. You can use your own text in footer. can include copyright etc. (HTML supported) by find on Telegram in link to on Project-Id-Version: Raha v0.2
POT-Creation-Date: 2016-08-04 11:11+0430
PO-Revision-Date: 2016-08-04 11:26+0430
Language-Team: Raha development team <me@poriya.ir>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
Last-Translator: 
Language: fa_IR
X-Poedit-SearchPath-0: .
 نوشته های قدیمی تر یک نوار شناور حاوی نمادک های شبکه های اجتماعی و پیوند به آنها که در سمت چپ-پایین صفحه قرار می‌گیرد.  برای نمایش ابزارک ها را اینجا اضافه کنید. متا تگ های اضافه در بخش HEAD دسته ها : ویرایش <i class="fa fa-pencil"></i> ایمیل نوارابزار اول پانوشت در گیت‌هاب دنبال کنید در لینکداین دنبال کنید در مدیم دنبال کنید در توییتر دنبال کنید متن پانوشت متن پانوشت و متا تگ ها گیت‌هاب فهرست سربرگ نام کاربری گیت هاب را بدون @ وارد کنید نام کاربری مدیم را بدون @ وارد کنید نام کاربری توییتر را بدون @ وارد کنید آدرس ایمیل خود را وارد کنید آدرس صفحه شخصی لینکداین را وارد کنید نام کاربری تلگرام خود را بدون @ وارد کنید ادامه نوشته <i class="fa fa-plus-circle"></i> لینکداین مدیم نوشته های جدید تر با افتخار قدرت گرفته از <a href="https://fa.wordpress.org"> وردپرس فارسی</a>. نوارابزار دوم پانوشت ایمیل ارسال کنید شبکه های اجتماعی متاسفم! نوشته را پیدا نکردم. متاسفم! پیدا نشد. برچسب ها : تلگرام نوار ابزار سوم پانوشت توییتر از نوار شبکه های اجتماعی استفاده کن مي‌‌توانید کد های اضافه خود را به بخش head صفحه خود اضافه کنید. برای مثال: متا تگ ها - کد های تایید گوگل - کد های شیوه‌نامه های اضافه و... می‌توانید متن موردنظر خودتان را در پانوشت بنویسید. این متن می‌تواند شامل کپی رایت یا چیز های دیگر باشد. (از اچ تی ام ال پشتیبانی می‌شود) توسط در تلگرام بیابید در پیوند به در 