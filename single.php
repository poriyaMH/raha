<?php
//This is the lay out of a single post
get_header();//Add the header of website from header.php
?>
    <main class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); //starts the loop ?>
            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                <?php if(has_post_format( 'quote' )) : // quote post format?>
                <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                <small class="postdate"><?php the_time('jS F, Y') ?></small>
                <div style="clear:both;"></div>
                <blockquote cite="<?php the_title(); ?>" class="post-quote">
                    <i class="fa fa-quote-right"></i><?php the_content(); ?><i class="fa fa-quote-left"></i>
                </blockquote>
                <span class="cite"><?php the_title(); ?></span>

            <?php elseif(has_post_format( 'image' )) : // image post format ?>
            <figure>
                <header>
                    <h2 class="post-title">
                        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e('link to', 'raha')?> <?php the_title_attribute(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h2>
                </header>
                <section>
                    <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                    <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail();
                        }
                    ?>
                </section>
                <?php
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
                 ?>
            </figure>

            <?php elseif(has_post_format( 'status' )) : // status post format ?>
                <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                <small class="postdate"><?php _e('on', 'raha'); ?> <?php the_time('jS F, Y') ?></small>
                <div style="clear:both"></div>
                <div class="status-content"><?php the_content(); ?></div>

            <?php else : // normal post ?>
            		<header>
                        <h2 class="post-title">
                        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e('link to', 'raha')?> <?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    </header>
                    <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                    <small class="postdate"><?php the_time('jS F, Y') ?>  <?php _e('by', 'raha'); ?> <?php the_author() ?></small>
                    <div style="clear:both"></div>
                    <?php // check if the post has a Post Thumbnail assigned to it.
                            if ( has_post_thumbnail() ) {
                            	the_post_thumbnail();
                            }
                    ?>
            		<?php the_content( __('Keep reading <i class="fa fa-plus-circle"></i>', 'raha') ); ?>
                    <small><i class="fa fa-pencil-square-o"></i> &nbsp; <?php _e('Categories', 'raha'); ?> <?php the_category('، ') ?> </small>
                    <?php if(has_tag()): ?>
                    <small class="tags-block"><i class="fa fa-tags"></i>&nbsp; <?php _e('Tags', 'raha'); ?> <?php the_tags( '', '، ', '' ); ?> </small>
                    <hr>
                    <!--starting the comments block-->
                    <?php
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                     ?>
                    <?php endif; ?>
            <?php endif;?><?php //the post format conditionals?>
            </article>
            <?php endwhile; // ends the while loop?>
            <?php else : //defines a condition whether the post is not found or deleted ?>
            	<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
            		<h2><?php _e('Sorry! Didn\'t find the post.', 'raha') ?> </h2>
            	</div>
            <?php endif; // ends the loop?>
    </main>
    <?php get_footer() ?>
<?php get_footer(); //add the footer section of the website ?>
