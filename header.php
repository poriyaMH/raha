<?php
//the First part of the theme. It defines the elementary parts of theme. Also starts the HTML markup and the header section.
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <!--jQuary javasript plugin commented for probable use
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        -->
        <!--Font awesome Added from CDN below-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"  crossorigin="anonymous">
        <!--Main theme stylesheet below-->
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css">
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <!--Ping back-->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/extra/html5.js"></script>
        <![endif]-->
        <!--Meta tags below-->
        <meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true);
        } else {
            bloginfo('name'); echo " - "; bloginfo('description');
        }
        ?>" />
        <?php wp_head(); // add the wordpress head tags ?>
	<?php echo get_theme_mod("meta-tags"); // this code adds the addintional meta tags added by user from Administration panel?>	
    </head>
    <body <?php body_class();// the classes of wordpress for body tag ?>>

    <!--defineing header-->
    <?php if( get_theme_mod("header-image-disable")) : // condition to disable header image if needed. it uses the wordpress costumization API. header-image-disable setting is defined in fuctions.php ?>
         <header id="header-no-image">
     <?php else : // if the user doesn't want to disable the header this normal tag will be used ?> 
         <header style="background-image: url('<?php header_image(); //the costum header image ?>');" id="header">
    <?php endif; ?>

     <!--main header sections-->
        <main id="header-container">
            <h1 id="logotype">
                <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name');?></a>
            </h1>

        <?php if( get_theme_mod("header-image-disable") ) : // Condition for the right color of texts in header navigation ?>
            <?php if( get_theme_mod("description-disable") == false) : // Condition to disable description if asked by user ?>
	        <h3 id="description-no-image"><?php bloginfo('description'); // shows the blog description while header image is disabled (with black font) ?></h3>
            <?php endif; ?>
	    <nav class="header-nav-container header-no-image-nav-container">
        <?php else : ?>
            <?php if( get_theme_mod("description-disable")  == false) : // Condition to disable description if asked by user ?>
	        <h3 id="description"><?php bloginfo('description'); // shows the blog description while header image is enabled (with white font) ?></h3>
            <?php endif; ?>
            <nav class="header-nav-container">
	<?php endif;?>

                <?php // adding the navigation for theme
                wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_class' => 'header-nav' ) );
                ?>
            </nav>
        </main>
    </header>
<?php //this php condition checks whether user wants to use the social icons from setting in Wordpress admin panel or not. it is defined by Wordpress costumizer API in functions.php file.?>

<?php if(get_theme_mod("icon-bar-active")):?>
<section class="social-icons"><!--social icons that are fixed on left bottom of site-->

		<?php if(get_theme_mod("twitter") !== ''): ?>
            <a href="https://twitter.com/@<?php echo get_theme_mod("twitter");?>" target="_blank" title="<?php _e('Follow on Twitter', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-circle-twitter fa-stack-1x fa-inverse"></i>
                    <i class="fa fa-twitter fa-stack-1x"></i>
                </span>
                </div>
            </a>
		<?php endif; ?>

		<?php if(get_theme_mod("medium") !== ''): ?>
            <a href="https://medium.com/@<?php echo get_theme_mod("medium");?>" target="_blank" title="<?php _e('Follow on Medium', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-circle-medium fa-stack-1x fa-inverse"></i>
                    <i class="fa fa-medium fa-stack-1x"></i>
                </span>
                </div>
            </a>
		<?php endif; ?>


		<?php if(get_theme_mod("github") !== ''): ?>
            <a href="https://github.com/<?php echo get_theme_mod("github");?>" target="_blank" title="<?php _e('Follow on Github', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg"><i class="fa fa-github fa-stack-1x"></i></span>
                </div>
            </a>
		<?php endif; ?>


		<?php if(get_theme_mod("email") !== ''): ?>
            <a href="mailto:<?php echo get_theme_mod("email");?>" title="<?php _e('Send Email to me', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-circle-email fa-stack-1x fa-inverse"></i>
                    <i class="fa fa-envelope fa-stack-1x"></i>
                </span>
                </div>
            </a>
		<?php endif; ?>


		<?php if(get_theme_mod("linkedin") !== ''): ?>
            <a href="<?php echo get_theme_mod("linkedin");?>" target="_blank" title="<?php _e('Follow on Linkedin', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-circle-linkedin fa-stack-1x fa-inverse"></i>
                    <i class="fa fa-linkedin fa-stack-1x"></i>
                </span>
                </div>
            </a>
		<?php endif; ?>


		<?php if(get_theme_mod("telegram") !== ''): ?>
            <a href="https://telegram.me/<?php echo get_theme_mod("telegram");?>" target="_blank" title="<?php _e('find on Telegram', 'raha') ?>">
                <div class="icon-container">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-circle-telegram fa-stack-1x fa-inverse"></i>
                    <i class="fa fa-paper-plane fa-stack-1x"></i>
                </span>
                </div>
            </a>
		<?php endif; ?>


    </section>
<?php endif; ?>

