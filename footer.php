<?php //this is the footer section of the theme. It defines the lay out of the footer that is presented in all pages
wp_footer();
?>
<footer id="site-footer"><!-- starting the footer section-->
    <section id="sidebars">
	<?php if ( is_active_sidebar( 'footer-sidebar1' ) ) : ?>
		<?php dynamic_sidebar( 'footer-sidebar1'); ?>
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'footer-sidebar2' ) ) : ?>
		<?php dynamic_sidebar( 'footer-sidebar2'); ?>
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'footer-sidebar3' ) ) : ?>
		<?php dynamic_sidebar( 'footer-sidebar3'); ?>
	<?php endif; ?>
    </section>
    <section id="footer-copyright">
        <p>
            <?php echo get_theme_mod("footer");?>
        </p>
    </section>
</footer>
</body><!--ends the body tag in header.php-->
