<?php
//This is the main theme file and All other files will be imported here.
//theme: Raha v0.2 - beta 2
get_header();//Add the header of website from header.php
?>
    <main class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); // starts the loop ?>
            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

            <?php if(has_post_format( 'quote' )) : // quote post format?>
                <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                <small class="postdate"><?php the_time('jS F, Y') ?></small>
                <div style="clear:both"></div>
                <blockquote cite="<?php the_title(); ?>" class="post-quote">
                    <i class="fa fa-quote-right"></i><?php the_content(); ?><i class="fa fa-quote-left"></i>
                </blockquote>
                <span class="cite"><?php the_title(); ?></span>

            <?php elseif(has_post_format( 'image' )) : // image post format ?>
            <figure>
                <header>
                    <h2 class="post-title">
                        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e('link to', 'raha')?> <?php the_title_attribute(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h2>
                </header>
                <section>
                    <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                    <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail();
                        }
                    ?>
                </section>
            </figure>

            <?php elseif(has_post_format( 'status' )) : // status post format ?>
                <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                <small class="postdate"> <?php _e('in', 'raha'); ?> <?php the_time('jS F, Y') ?></small>
                <div style="clear:both"></div>
                <div class="status-content"><?php the_content(); ?></div>

            <?php else : // normal post ?>
            		<header>
                        <h2 class="post-title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e('link to', 'raha')?> <?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    </header>
                    <?php edit_post_link( __('Edit <i class="fa fa-pencil"></i>', 'raha') ); // add the edit butten ?>
                    <small class="postdate"><?php the_time('jS F, Y') ?>  <?php _e('by', 'raha') ?> <?php the_author() ?></small>
                    <div style="clear:both"></div>
                    <?php // check if the post has a Post Thumbnail assigned to it.
                            if ( has_post_thumbnail() ) {
                            	the_post_thumbnail();
                            }
                    ?>
            		<?php the_content( __('Keep reading <i class="fa fa-plus-circle"></i>', 'raha') ); ?>
            <?php endif; //the post format conditionals?>
            </article>

            <?php endwhile; //ends the while loop?>

        <?php else : // a condition for when we have no posts in the loop?>

            	<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
            		<h2><?php _e('Sorry! not found.', 'raha') ?></h2>
            	</div>

            <?php endif;//ends the loop ?>
    </main>
    <nav class="post-navigation">
        <div class="container">
            <div class="newerposts"><?php previous_posts_link( __('Newer Posts &raquo;', 'raha') )?></div>
            <div class="olderposts"><?php next_posts_link( __('&laquo; Older Posts', 'raha') )?></div>
            <div style="clear:both;"></div>
        </div>
    </nav>
<?php get_footer(); //add the footer section of the website ?>
